import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import Products from "./components/Products";
import Login from "./components/Login";
import Detail from "./components/Detail";
import Category from "./components/category/index";
function App() {
  return (
    <Suspense fallback="loading">
      <Router>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route exact path="/products/" component={Products} />
          <Route path="/login" component={Login} />
          <Route path="/category" component={Category} />
          <Route path="/detail" component={Detail} />
        </Switch>
      </Router>
    </Suspense>
  );
}

export default App;
