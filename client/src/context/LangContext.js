import React, { createContext, useState } from "react";

export const LangContext = createContext();

const LanguageContext = (props) => {
  const [state, setState] = useState({
    isLogged: false,
    lang: "en",
    api: "",
  });
  const Login = () => {
    setState({ ...state, isLogged: !state.isLogged });
  };

  const ChangeLang = (lang) => {
    setState({ ...state, lang: lang });
  };
  return (
    <LangContext.Provider value={{ ...state, Login, ChangeLang }}>
      {props.children}
    </LangContext.Provider>
  );
};

export default LanguageContext;
