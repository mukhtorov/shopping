import styled from "styled-components";
import { StyledIconBase } from "@styled-icons/styled-icon";

export const Container = styled.div`
  /* position: relative; */
  display: none;
  width: 360px;
  /* height: 97%; */
  bottom: 0;
  flex-direction: row;
  flex-wrap: wrap;
  /* background-color: red; */
  padding-left: 10px;
  border-radius: 5px;
  @media (max-width: 415px) {
    display: flex;
  }
  @media (max-width: 360px) {
    padding: 0px;
    display: flex;
  }
`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 20px;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  width: 86px;
`;
export const MenuContent = styled.div`
  display: flex;
  flex-direction: row;
  padding-bottom: 10px;
  margin-right: 10px;
  background-color: #f2f2f2;
  border-radius: 5px;
  cursor: pointer;
  align-items: center;
  padding-top: 5px;
  margin-bottom: 10px;
`;

Content.Title = styled.text`
  /* color: (props)=>{props.isSelected} ; */
  /* padding-left: 10px; */
  font-size: 10px;
`;

Content.Icon = styled.text`
  color: black;
  padding-right: 10px;
`;

export const IconStyleWrapper = styled.div`
  ${StyledIconBase} {
    color: white;
    font-size: 14;
    size: 14;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    padding: 5px;
    background-color: #f2a53f;
  }
`;
