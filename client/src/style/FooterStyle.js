import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: 250px;
  bottom: 0;
  flex-direction: row;
  padding-left: 150px;
  padding-right: 150px;
  padding-top: 50px;
  padding-bottom: 50px;
  flex: 1;
  justify-content: space-between;
  background-color: #e6e6e6;
  @media (max-width: 415px) {
    /* width: 200px; */
    flex-wrap: wrap;
    padding: 15px;
    height: 400px;
  }
`;

export const Title = styled.text`
  display: flex;
  font-size: 20px;
  color: #000;
  font-family: Open Sans, Arial, Helvetica, sans-serif, SimSun, 宋体;
  line-height: 1.3;
`;

export const Description = styled.text`
  display: flex;
  font-size: 13px;
  color: #999;
  line-height: 18px;
`;
export const Right = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 50px;
  flex: 1;
  @media (max-width: 415px) {
    padding-right: 15px;
  }
`;
export const Left = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 0;
  padding-right: 50px;
  @media (max-width: 415px) {
    padding-right: 15px;
  }
`;
export const Segment = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 50px;
  @media (max-width: 415px) {
    height: 170px;
  }
`;
