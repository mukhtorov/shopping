import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: 300px;
  bottom: 0;
  flex-direction: row;
  padding-left: 150px;
  padding-right: 150px;
  padding-top: 50px;
  padding-bottom: 50px;
  flex: 1;
  justify-content: space-between;
  background-color: white;
  @media (max-width: 415px) {
    display: none;
  }
`;
export const Box = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #eee;
  padding-right: 5px;
  padding-left: 5px;
  /* padding-top: 10px; */
  padding-bottom: 20px;
  justify-content: center;
  align-items: center;
`;
export const Title = styled.text`
  display: flex;
  font-size: 16px;
  color: #000;
  font-family: Open Sans, Arial, Helvetica, sans-serif, SimSun, 宋体;
  line-height: 1.3;
`;

export const Description = styled.text`
  display: flex;
  font-size: 13px;
  color: #999;
  line-height: 18px;
  justify-content: center;
  align-items: center;
`;
