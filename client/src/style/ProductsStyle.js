import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: auto;
  /* background-color: pink; */
  padding-left: 80px;
  padding-right: 80px;
  padding-top: 15px;
  flex-wrap: wrap;
  justify-content: space-around;
  @media (max-width: 415px) {
    padding: 10px;
  }
`;

export const Item = styled.div`
  display: flex;
  background-color: white;
  flex-direction: column;
  border: 1px solid #b86e00;
  width: 193px;
  align-items: center;
  height: auto;
  border-radius: 5px;
  margin-right: 16px;
  margin-bottom: 10px;
  @media (max-width: 415px) {
    margin-right: 5px;
    width: 170px;
  }
  @media (max-width: 375px) {
    margin-right: 5px;
    width: 155px;
  }
`;
Item.Offer = styled.text`
  display: flex;
  align-items: center;
  justify-content: center;
  color: red;
  width: 80px;
  background: #ffecec;
  font-size: 14px;
  margin-left: 15px;
  @media (max-width: 415px) {
    width: 50px;
    font-size: 12px;
    height: 20px;
  }
`;

export const Images = styled.div`
  margin-top: 5px;
  border-radius: 5px;
  width: 150px;
  height: 150px;
  background-color: pink;
  @media (max-width: 415px) {
    font-size: 16px;
  }
`;
Images.img = styled.img`
  width: 150px;
  height: 150px;
  /* @media (max-width: 415px) {
    width: 130px;
    height: 130px;
  } */
  /* @media (max-width: 375px) {
    height: 120px;
    width: 120px;
  } */
`;
Item.Title = styled.text`
  display: flex;
  color: black;
  padding-top: 10px;
  padding-bottom: 10px;
  font-weight: 800;
  /* margin-top: 15px; */
  @media (max-width: 415px) {
    padding: 5px;
  }
`;
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;
  @media (max-width: 415px) {
    margin-top: 5px;
  }
`;

Item.Desc = styled.div`
  display: flex;
  font-size: 14px;
  padding: 10px;
  @media (max-width: 415px) {
    font-size: 12px;
    padding: 1px 5px 10px 5px;
  }
`;
