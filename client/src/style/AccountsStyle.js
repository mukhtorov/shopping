import styled from "styled-components";
import { Shopify } from "@styled-icons/fa-brands/Shopify";
import { Shoppify } from "./NavbarStyle";

export const Container = styled.div`
  display: flex;
  height: 90%;
  width: 400px;
  bottom: 0;
  flex-direction: column;
  background-color: white;
  margin-top: 10px;
  margin-bottom: 10px;
  border-radius: 5px;
  align-items: center;
  padding-top: 60px;
  @media (max-width: 415px) {
    display: none;
  }
  /* justify-content:center */
`;

export const User = styled.div`
  display: flex;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  background-color: #f2f2f2;
`;

User.Title = styled.text`
  color: bold black;
  font-weight: 800;
  padding-top: 10px;
`;

User.Description = styled.text`
  color: #6c6c70;
  font-size: 14px;
`;

export const Buttons = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 25px;
  padding-bottom: 25px;
`;

Buttons.Telegram = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  width: 30px;
  font-weight: 800;
  height: 30px;
  background-color: #0088cc;
  margin-right: 10px;
  padding: 5px;
  /* padding-right: 15px;
  padding-left: 15px; */
  border-radius: 50%;
  cursor: pointer;
`;
Buttons.Instagram = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  height: 30px;
  width: 30px;
  padding: 5px;
  background-color: #c13584;
  margin-right: 10px;
  cursor: pointer;
  border-radius: 50%;
`;
Buttons.Whatsapp = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  height: 30px;
  width: 30px;
  margin-right: 10px;
  padding: 5px;
  background-color: #25d366;
  cursor: pointer;
  border-radius: 50%;
`;

Buttons.FacebookCircle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  height: 30px;
  width: 30px;
  padding: 5px;
  background-color: #3b5998;
  cursor: pointer;
  border-radius: 50%;
`;

export const Special = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #c13584;
  width: 85%;
  height: 45%;
  margin: 5px;
  border-radius: 5px;
  padding: 10px;
`;
Special.Title = styled.text`
  display: flex;
  color: white;
  font-weight: 600;
`;

export const Shoppiffy = styled(Shoppify)`
  display: flex;
  color: white;
  margin: auto;
`;
