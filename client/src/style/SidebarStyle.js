import styled from "styled-components";
import { StyledIconBase } from "@styled-icons/styled-icon";

export const Container = styled.div`
  display: flex;
  width: 400px;
  height: 97%;
  bottom: 0;
  flex-direction: column;
  background-color: white;
  padding-left: 10px;
  border-radius: 5px;
  @media (max-width: 415px) {
    display: none;
  }
`;
export const Content = styled.div`
  display: flex;
  flex-direction: row;
  padding-bottom: 20px;
  cursor: pointer;
`;
export const MenuContent = styled.div`
  display: flex;
  flex-direction: row;
  padding-bottom: 10px;
  margin-right: 10px;
  background-color: #f2f2f2;
  border-radius: 5px;
  cursor: pointer;
  align-items: center;
  padding-top: 5px;
  margin-bottom: 10px;
`;

Content.Title = styled.text`
  color: (props)=>{props.isSelected} ;
  padding-left: 10px;
`;

Content.Icon = styled.text`
  color: black;
  padding-right: 10px;
`;

export const IconStyleWrapper = styled.div`
  ${StyledIconBase} {
    color: #a1a1a1;
    font-size: 24;
    size: 24;
  }
`;
