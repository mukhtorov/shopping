import styled from "styled-components";
import { Shopify } from "@styled-icons/fa-brands/Shopify";
import { SearchAlt } from "@styled-icons/boxicons-regular/SearchAlt";

export const Container = styled.div`
  background-color: white;
  padding-right: 150px;
  padding-left: 145px;
  padding-top: 50px;
  padding-bottom: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  height: 60px;
  @media (max-width: 415px) {
    padding: 10px;
  }
`;
export const SearchContainer = styled.div`
  display: flex;
  width: 100%;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 36px;
  padding-left: 20px;
  padding-right: 20px;
  margin-left: 20px;
  border: 2px solid #b86e00;
  border-right: 0 none;
  border-radius: 4px 0 0 4px;
  @media (max-width: 415px) {
    padding-left: 10px;
    margin-left: 1px;
  }
`;

export const SearchBox = styled.div`
  z-index: 5;
  right: 10;
  top: 0;
  width: 42px;
  height: 36px;
  background-color: #b86e00;
  border-radius: 0 4px 4px 0;
  cursor: pointer;
  margin-right: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 415px) {
    margin-right: 0px;
  }
`;
export const SearchIcon = styled(SearchAlt)`
  color: white;
`;
export const Logo = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  padding-right: 10px;
  color: red;
  .mobileLogo {
    @media (max-width: 415px) {
      flex-direction: row;
      color: red;
    }
  }
  @media (max-width: 415px) {
    width: 150px;
  }
  @media (max-width: 375px) {
    font-size: 16px;
    width: 105px;
    padding-right: 0px;
  }
`;
Logo.Title = styled.text`
  font-size: 28px;
  color: #b86e00;
  @media (max-width: 415px) {
    font-size: 18px;
  }
  @media (max-width: 375px) {
    font-size: 16px;
  }
`;
Logo.Description = styled.text`
  font-size: 14px;
  color: #999;
  @media (max-width: 415px) {
    font-size: 12px;
  }
  @media (max-width: 375px) {
    font-size: 6px;
  }
`;
export const Shoppify = styled(Shopify)`
  size: 22;
  color: #b86e00;
  align-items: center;
  justify-content: center;
`;

export const MenuToggle = styled.div`
  z-index: 5;
  right: 10;
  top: 0;
  width: 300px;
  height: 36px;
  border: 2px solid #b86e00;
  border-right: 0 none;
  border-left: 0 none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 415px) {
    display: none;
  }
`;

export const MenuList = styled.div`
  display: flex;
  height: auto;
  width: 150px;
  background-color: black;
  flex-direction: column;
  padding-left: 20px;
  padding-top: 5px;
  padding-bottom: 5px;
  padding-right: 20px;
  opacity: 0.7;
  outline: 0 none;
  visibility: visible;
  position: absolute;
  justify-content: flex-end;
  float: right;
  right: 235px;
  top: 127px;
`;

MenuList.Item = styled.text`
  color: white;
  cursor: pointer;
`;

export const VerticalLine = styled.div`
  height: 36px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-right: 1px solid #ccc;
`;

export const ShoppingIcon = styled.div`
  display: flex;
  @media (max-width: 415px) {
    display: none;
  }
`;
