import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  padding: 150px;
  @media (max-width: 415px) {
    flex-direction: column;
    padding: 10px;
    margin-bottom: 50px;
  }
`;

Container.Right = styled.div`
  display: flex;
  flex-direction: row;
  padding-right: 50px;
  @media (max-width: 415px) {
    padding: 20px;
    justify-content: center;
  }
`;
Container.Left = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Back = styled.div`
  display: flex;
  position: absolute;
  top: 30px;
  left: 30px;
  @media (max-width: 415px) {
    top: 10px;
    left: 10px;
  }
`;

export const Data = styled.div`
  display: flex;
`;
Data.Title = styled.text`
  font-size: 26px;
  font-weight: 800;
`;
Data.Image = styled.img`
  width: "350px";
  height: "400px";
  border-radius: 10px;
`;
Data.ContentTitle = styled.text`
  font-size: 16px;
  font-weight: 800;
  padding-bottom: 10px;
`;
Data.Description = styled.text`
  margin-top: 15px;
  font-size: 18px;
  color: #595959;
`;
Data.Line = styled.div`
  padding-top: 1px;
  background-color: #c9c9c9;
  margin-top: 10px;
  margin-bottom: 10px;
`;
export const Color = styled.div`
  display: flex;
  flex-direction: row;
`;
Color.Images = styled.img`
  width: 80px;
  height: 80px;
  border-radius: 10px;
  margin-right: 10px;
  /* border: 1px solid red; */
  border: 2px solid ${(props) => props.color};
  @media (max-width: 415px) {
    width: 70px;
    height: 80px;
    border: 1px solid ${(props) => props.color};
  }
`;

export const Buttons = styled.div`
  display: flex;
  flex-direction: row;
  /* padding-top: 35px;
  padding-bottom: 35px; */
`;

Buttons.Telegram = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  width: 50px;
  height: 50px;
  font-weight: 800;
  background-color: #0088cc;
  margin-right: 10px;
  padding: 5px;
  /* padding-right: 15px;
  padding-left: 15px; */
  border-radius: 50%;
  cursor: pointer;
`;
Buttons.Instagram = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  width: 50px;
  height: 50px;
  padding: 5px;
  background-color: #c13584;
  margin-right: 10px;
  cursor: pointer;
  border-radius: 50%;
`;
Buttons.Whatsapp = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  width: 50px;
  height: 50px;
  margin-right: 10px;
  padding: 5px;
  background-color: #25d366;
  cursor: pointer;
  border-radius: 50%;
`;

Buttons.FacebookCircle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-weight: 800;
  width: 50px;
  height: 50px;
  padding: 5px;
  background-color: #3b5998;
  cursor: pointer;
  border-radius: 50%;
`;
