import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  height: 450px;
  bottom: 0;
  flex-direction: row;
  padding-left: 150px;
  padding-right: 150px;
  flex: 1;
  justify-content: space-between;
  @media (max-width: 415px) {
    height: 500px;
    padding: 10px;
    /* flex-direction: column; */
  }
`;

export const BodyContainer = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  flex-direction: column;
  background-color: inherit;
  color: white;
  border: 5px solid inherit;
  border-radius: 10px;
`;

export const BodyCarousel = styled.div`
  /* position: fixed; */
  display: flex;
  height: 94%;
  flex-direction: rows;
  color: white;
  border: 1px solid white;
  border-radius: 5px;
  margin: 10px;
  @media (max-width: 415px) {
    width: 395px;
    flex-direction: column;
    margin: 0px;
    height: 80%;
  }
  @media (max-width: 375px) {
    width: 356px;
    flex-direction: column;
    margin: 0px;
  }
  @media (max-width: 360px) {
    width: 330px;
    flex-direction: column;
    margin: 0px;
    height: 85%;
  }
  @media (max-width: 320px) {
    width: 350px;
    flex-direction: column;
    margin: 0px;
    height: 85%;
  }
`;

BodyCarousel.Content = styled.div`
  display: flex;
  /* width: 500px; */
  height: 300px;
  overflow: hidden;
  @media (max-width: 415px) {
    height: 200px;
  }
  @media (max-width: 375px) {
    height: 200px;
  }
`;

export const BodyDeal = styled.div`
  display: flex;
  height: 60%;
  bottom: 0;
  flex-direction: row;
  background-color: white;
  color: white;
  border: 1px solid white;
  border-radius: 5px;
  /* margin-right: 10px; */
  margin-left: 10px;
  margin-bottom: 10px;
`;

export const OfferBox = styled.div`
  display: flex;
  /* background-color: red; */
  flex-direction: column;
  border-right: 1px solid #e3e3e3;
  width: 130px;
  align-items: center;
  /* justify-content: center; */
`;
OfferBox.Offer = styled.text`
  /* position: absolute; */
  bottom: 164px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  border-radius: 20px;
  width: 80px;
  background-color: red;
  font-size: 14px;
  /* font-family: 1 */
`;

OfferBox.Img = styled.div`
  margin-top: 5px;
  border-radius: 5px;
  width: 110px;
  height: 110px;
  background-color: pink;
`;

OfferBox.Title = styled.text`
  display: flex;
  color: black;
  margin-top: 15px;
`;
