import styled from "styled-components";

export const Container = styled.div`
  height: 40px;
  border-bottom: 1px solid #f2f2f2;
  background-color: #fafafa;
  font-size: 12px;
  display: flex;
  padding-left: 200px;
  padding-right: 150px;
  justify-content: flex-end;
  vertical-align: baseline;
  align-content: center;
  align-items: center;
  @media (max-width: 415px) {
    padding: 10px;
  }
`;
export const Label = styled.text`
  font-size: 12px;
  vertical-align: baseline;
  padding-left: 5px;
`;
export const VerticalLine = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
  margin-right: 10px;
  margin-left: 10px;
  border-right: 1px solid #ccc;
  top: 0;
`;
