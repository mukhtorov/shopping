import React from "react";
import {
  Container,
  Right,
  Left,
  Title,
  Description,
  Segment,
} from "../style/FooterStyle";

const Footer = () => {
  return (
    <Container>
      <Left>
        <Segment>
          <Title>Help</Title>
          <Description>
            Customer Service, Disputes & Reports, Buyer Protection, Report IPR
            infringement
          </Description>
        </Segment>
        <Segment>
          <Title>Browse by Category</Title>
          <Description>
            All Popular, Product, Promotion, Low Price, Great Value, Reviews,
            Blog, Seller Portal, BLACK FRIDAY, Coronavirus
          </Description>
        </Segment>
      </Left>
      <Right>
        <Segment>
          <Title>DXB Bazar Multi-Language Sites</Title>
          <Description>Russian, Uzbek, English, Korean, Arabic</Description>
        </Segment>
        <Segment>
          <Title>DXB Bazar Group</Title>
          <Description>
            DXB Bazar Group Website, DXB Bazar, Alimama, Alipay, Fliggy, Alibaba
            Cloud, Alibaba International.
          </Description>
        </Segment>
      </Right>
    </Container>
  );
};

export default Footer;
