import React, { Component } from "react";
import styled from "styled-components";
import { ArrowBack } from "@styled-icons/boxicons-regular/ArrowBack";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <Container>
      <Back>
        <Link to="/">
          <ArrowBack size={24} color="black" />
        </Link>
      </Back>
      <Input placeholder="User name" />
      <Input type="password" placeholder="Password" />
      <Button
        onClick={() => {
          alert("You are not authorized to login");
        }}
      >
        {" "}
        Login
      </Button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  height: 800px;
  /* margin: auto; */
  align-items: center;
  justify-content: center;
  /* background: red; */
  flex-direction: column;
  @media (max-width: 415px) {
    height: 500px;
  }
`;
const Input = styled.input`
  width: 250px;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 10px;
  background-color: white;
`;

const Button = styled.button`
  width: 250px;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 10px;
`;
const Back = styled.div`
  display: flex;
  position: absolute;
  top: 30px;
  left: 30px;
`;
export default Login;
