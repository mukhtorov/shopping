import React from "react";
import { Container, Title, Description, Box } from "../style/ProcessStyle";
import { MoneyDollarCircle } from "@styled-icons/remix-line/MoneyDollarCircle";
import { CreditCardOutline } from "@styled-icons/evaicons-outline/CreditCardOutline";
import { Truck } from "@styled-icons/remix-line/Truck";
import { SecurePayment } from "@styled-icons/remix-line/SecurePayment";
import { PeopleOutline } from "@styled-icons/material/PeopleOutline";
import { PlayStore } from "@styled-icons/boxicons-logos/PlayStore";
const Process = () => {
  return (
    <Container>
      <Box>
        <MoneyDollarCircle size={56} color="#485c69" />
        <Title>Great Value</Title>
        <Description>
          We offer competitive prices on our 100 million plus product range.
        </Description>
      </Box>
      <Box>
        <Truck size={56} color="#485c69" />
        <Title>Worldwide Delivery</Title>
        <Description>
          With sites in 5 languages, we ship to over 200 countries & regions.
        </Description>
      </Box>
      <Box>
        <CreditCardOutline size={56} color="#485c69" />
        <Title>Safe Payment</Title>
        <Description>
          With sites in 5 languages, we ship to over 200 countries & regions.
        </Description>
      </Box>
      <Box>
        <SecurePayment size={46} color="#485c69" />
        <Title>Shop with Confidence</Title>
        <Description>
          With sites in 5 languages, we ship to over 200 countries & regions.
        </Description>
      </Box>
      <Box>
        <PeopleOutline size={56} color="#485c69" />
        <Title>24/7 Help Center</Title>
        <Description>
          With sites in 5 languages, we ship to over 200 countries & regions.
        </Description>
      </Box>
      <Box>
        <PlayStore size={56} color="#485c69" />
        <Title>Shop On-The-Go</Title>
        <Description>
          With sites in 5 languages, we ship to over 200 countries & regions.
        </Description>
      </Box>
    </Container>
  );
};

export default Process;
