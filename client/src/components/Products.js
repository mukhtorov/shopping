import React, { useState, useEffect } from "react";
import { Container, Item, Wrapper, Images } from "../style/ProductsStyle";
import { ProductItem } from "../moc/Items";
import { Link } from "react-router-dom";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

const Products = (props) => {
  const [img, setImg] = useState("");
  const [loading, setLoading] = useState(true);
  const [imgData, setImgData] = useState([]);
  const [route, setRoute] = useState('')
  console.log("props.route", props)
  // setRoute
  useEffect(() => {

    setRoute(props?.route)
    fetch(`https://dxbuz.herokuapp.com/api/${route}`)
    .then((response) => response.json())
    .then((data) => {
      setImgData(data.resources);
      setLoading(false);
      console.log("json data", data.resources);
    })
    .catch((error) => {
      console.log("smd", error);
    });
    
  }, [route]);
  return (
    <>
      <Container>
        {
          loading ? (
            <Loader
              type="ThreeDots"
              color="#b86e00"
              height={100}
              width={100}
              visible={loading}
            />
          ) : (
            imgData.map((item) => {
              return (
                <Item key={item.public_id}>
                  <Link
                    to={{
                      pathname: "/detail",
                      state: {
                        item: item,
                        title: item.context?.custom?.caption,
                        description:item.context?.custom?.alt,
                        price: item.context?.custom?.price,
                        size: item.context?.custom?.size?.split(' '),
                        color: item.context?.custom?.color?.split(' '),
                      },
                    }}
                  >
                    <Images>
                      <Images.img src={item.url} alt="img"></Images.img>
                    </Images>
                  </Link>
                  <Wrapper>
                    <Item.Title>{item.context?.custom?.price}</Item.Title>
                  <Item.Offer>{item.context?.custom?.discount}</Item.Offer>
                  </Wrapper>
                  <Item.Title>{item.context?.custom?.caption}</Item.Title>
                  <Item.Desc>
                      {item.context?.custom?.alt}
                  </Item.Desc>
                </Item>
              );
            })
  )
        }
      </Container>
    </>
  );
};

export default Products;
