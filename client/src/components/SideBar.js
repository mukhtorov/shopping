import React, { useState, useContext } from "react";
import {
  Container,
  Content,
  IconStyleWrapper,
  MenuContent,
} from "../style/SidebarStyle";
import { Man } from "@styled-icons/icomoon/Man";
import { LaptopMac } from "@styled-icons/material/LaptopMac";
import { JediOrder } from "@styled-icons/fa-brands/JediOrder";
import { Home } from "@styled-icons/boxicons-regular/Home";
import { ShoppingBags } from "@styled-icons/boxicons-solid/ShoppingBags";
import { ChildFriendly } from "@styled-icons/material-outlined/ChildFriendly";
import { Basketball } from "@styled-icons/boxicons-regular/Basketball";
import { Woman } from "@styled-icons/icomoon/Woman";
import { DonateHeart } from "@styled-icons/boxicons-regular/DonateHeart";
import { Motorcycle } from "@styled-icons/material-outlined/Motorcycle";
import { CarMechanic } from "@styled-icons/boxicons-solid/CarMechanic";
import { Menu2Outline } from "@styled-icons/evaicons-outline/Menu2Outline";
import { Fastfood } from "@styled-icons/material-outlined/Fastfood";
import { useTranslation } from "react-i18next";
import { LangContext } from "../context/LangContext";
import { Link } from "react-router-dom";

const Body = () => {
  const data = useContext(LangContext);
  const { lang, ChangeLang, Login } = data;
  const { t } = useTranslation(lang);

  const [menu, setMenu] = useState("");
  return (
    <Container>
      <MenuContent>
        <IconStyleWrapper>
          <Menu2Outline size={24} />
        </IconStyleWrapper>
        <Content.Title>{t("menu.all")}</Content.Title>
      </MenuContent>
      {/* <Link to="/category" query={{ title: t("menu.men") }}> */}
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.men"),
            route: 'women'
          },
        }}
      >
        <Content>
          <IconStyleWrapper>
            <Man size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.men")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.women"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <Woman size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.women")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.laptop"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <LaptopMac size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.laptop")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.watch"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <JediOrder size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.watch")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.bag"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <ShoppingBags size={24} />
          </IconStyleWrapper>
          <Content.Title> {t("menu.bag")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.sport"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <Basketball size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.sport")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.health"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <DonateHeart size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.health")}</Content.Title>
        </Content>
      </Link>
      <Link
        to={{
          pathname: "/category",
          state: {
            state:t("menu.food"),
            route: 'women'
          },
        }}
      >
        {" "}
        <Content>
          <IconStyleWrapper>
            <Fastfood size={24} />
          </IconStyleWrapper>
          <Content.Title>{t("menu.food")}</Content.Title>
        </Content>
      </Link>
    </Container>
  );
};

export default Body;
