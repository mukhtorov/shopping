import React from "react";
import { ArrowBack } from "@styled-icons/boxicons-regular/ArrowBack";
import { Link } from "react-router-dom";
import { Container, Back, Data, Color, Buttons } from "../style/DetailStyle";
import { Telegram } from "@styled-icons/remix-line/Telegram";
import { Instagram } from "@styled-icons/entypo-social/Instagram";
import { Whatsapp } from "@styled-icons/fa-brands/Whatsapp";
import { FacebookCircle } from "@styled-icons/boxicons-logos/FacebookCircle";
import ReactImageZoom from "react-image-zoom";

const Detail = (props) => {
  const { item, title, description, price, size, color } = props.location.state;
  const propss = { width: 400, height: 400, zoomWidth: 500, img: item.url };
  return (
    <Container>
      <Back>
        <Link to="/">
          <ArrowBack size={24} color="black" />
        </Link>
      </Back>
      <Container.Right>
        <ReactImageZoom {...propss} />
        {/* <Data.Image
          style={{
            width: "400px",
            height: "450px",
          }}
          src={item.url}
          alt="img"
        ></Data.Image> */}
      </Container.Right>
      <Container.Left>
        <Data.Title>{title}</Data.Title>
        <Data.Line />
        <Data.Description>{description}</Data.Description>
        <Data.Line />
        <Data.ContentTitle>Colors:</Data.ContentTitle>
        <Color>
          {color?color.map((clr) => {
            return <Color.Images src={item.url} color={clr} />;
          }): <p>not defined</p>}
        </Color>
        <Data.Line />
        <Data.ContentTitle>Size:</Data.ContentTitle>
        <div style={{display: "flex", flexDirection: 'row'}}>

        {size?size.map((clr) => {
          return  <p style={{paddingRight: '10px'}}>{clr}</p>
        }): <p>not defined</p>}
        </div>
        <Data.Line />
        <Data.ContentTitle>Price:</Data.ContentTitle>
        <p style={{ color: "red" }}>{price}</p>
        <Data.Line />
        <Data.ContentTitle>Buy Now</Data.ContentTitle>
        <Buttons>
          <a href="http://t.me/dxbbazaruzb">
            <Buttons.Telegram>
              <Telegram size={66} />
            </Buttons.Telegram>
          </a>
          <a href=" https://www.instagram.com/dxbbazaruzb/">
            <Buttons.Instagram>
              <Instagram size={30} />
            </Buttons.Instagram>
          </a>
          <a href=" https://www.instagram.com/dxbbazaruzb/">
            <Buttons.Whatsapp>
              <Whatsapp size={35} />
            </Buttons.Whatsapp>
          </a>
          <a href=" https://www.instagram.com/dxbbazaruzb/">
            <Buttons.FacebookCircle>
              <FacebookCircle size={35} />
            </Buttons.FacebookCircle>
          </a>
        </Buttons>
      </Container.Left>
    </Container>
  );
};

export default Detail;
