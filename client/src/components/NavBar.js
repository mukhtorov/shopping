import React, { useContext } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import {
  Container,
  SearchInput,
  Logo,
  SearchContainer,
  Shoppify,
  SearchBox,
  SearchIcon,
  MenuToggle,
  VerticalLine,
  ShoppingIcon,
} from "../style/NavbarStyle";

import "react-dropdown/style.css";
import { useTranslation } from "react-i18next";
import { Dropdown } from "semantic-ui-react";
import { LangContext } from "../context/LangContext";

const NavBar = () => {
  const data = useContext(LangContext);
  const { isLogged, lang, ChangeLang, Login } = data;
  const { t, i18n } = useTranslation(lang);
  return (
    <Container>
      <Logo>
        <Link to="/">
          <Logo.Title>{t("navbar.title")}</Logo.Title>
        </Link>
        {/* <Link to="/">
          <Logo.Description>{t("navbar.description")}</Logo.Description>
        </Link> */}
      </Logo>
      <SearchContainer>
        {/* <SearchInput placeholder={t("navbar.description")}></SearchInput> */}
        <SearchInput placeholder="Search your product"></SearchInput>
        <VerticalLine />

        <MenuToggle>
          <Dropdown
            compact
            deburr
            placeholder={t("menu.all")}
            options={[
              {
                key: "0",
                text: t("menu.all"),
                value: "all",
              },
              {
                key: "1",
                text: t("menu.men"),
                value: "men",
              },
              {
                key: "2",
                text: t("menu.women"),
                value: "women",
              },
              {
                key: "3",
                text: t("menu.laptop"),
                value: "computer",
              },
              {
                key: "4",
                text: t("menu.watch"),
                value: "watches",
              },
              {
                key: "6",
                text: t("menu.bag"),
                value: "bag",
              },
              {
                key: "7",
                text: t("menu.sport"),
                value: "sprot",
              },
              {
                key: "8",
                text: t("menu.health"),
                value: "health",
              },
              {
                key: "9",
                text: t("menu.food"),
                value: "food",
              },
            ]}
          />
        </MenuToggle>
        <SearchBox>
          <SearchIcon size={22} />
        </SearchBox>
      </SearchContainer>
      <ShoppingIcon>
        <Shoppify size={40} />
      </ShoppingIcon>
    </Container>
  );
};

export default NavBar;
