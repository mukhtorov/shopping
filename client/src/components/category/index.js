import React, { useContext } from "react";
import Products from "../Products";
import {
  Container,
  SearchInput,
  Logo,
  SearchContainer,
  Shoppify,
  SearchBox,
  SearchIcon,
  VerticalLine,
  ShoppingIcon,
} from "./indexStyle";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { LangContext } from "../../context/LangContext";

const Home = (props) => {
  const data = useContext(LangContext);
  const { lang } = data;
  const { t } = useTranslation(lang);
  console.log("lllls", props)
  return (
    <div>
      <Container>
        <Logo>
          <Link style={{ width: "max-content" }} to="/">
            <Logo.Title>{props.location.state.state}</Logo.Title>
          </Link>
          <Link to="/">
            <Logo.Description>Smart Shopping Better Living</Logo.Description>
          </Link>
        </Logo>
        <SearchContainer>
          <SearchInput placeholder="Search your product"></SearchInput>
          <VerticalLine />
          <SearchBox>
            <SearchIcon size={22} />
          </SearchBox>
        </SearchContainer>
        <ShoppingIcon>
          <Shoppify size={40} />
        </ShoppingIcon>
      </Container>
      <Products route={props.location.state.route} />
    </div>
  );
};

export default Home;
