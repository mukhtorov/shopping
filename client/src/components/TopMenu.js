import React, { Component, useState, useContext, useEffect } from "react";
import { Container, Label, VerticalLine } from "../style/TopMenuStyle";
// import { User } from "@styled-icons/fa-regular/User";
import { User } from "@styled-icons/evil/User";
import { LangContext } from "../context/LangContext";
import { Dropdown, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { useTranslation } from "react-i18next";

const Topmenu = () => {
  const data = useContext(LangContext);
  const { isLogged, lang, ChangeLang, Login } = data;
  const { t, i18n } = useTranslation(lang);

  const ChangeLangLocal = (localLan) => {
    const [state, setState] = useState(localStorage.getItem(localLan));

    const setLocal = (newItem) => {
      localStorage.setItem(localLan, newItem);
      setState(newItem);
      window.location.reload(false);
    };
    return [state, setLocal];
  };
  const [ln, setLn] = ChangeLangLocal("en");
  useEffect(() => {
    ChangeLang(ln);
  }, []);

  return (
    <Container>
      {/* <Label>Help / Buyer Protection</Label>
      <VerticalLine class="vl" /> */}
      <Label>
        <Dropdown
          onChange={(e, { value }) => {
            // setLn(value);
            setLn(value);
            // window.location.reload(true);
          }}
          compact
          deburr
          placeholder="English"
          options={[
            {
              key: "1",
              text: "English",
              value: "en",
            },
            {
              key: "2",
              text: "Arabic",
              value: "ar",
            },
            {
              key: "3",
              text: "Korean",
              value: "kr",
            },
            {
              key: "4",
              text: "Uzbek",
              value: "uz",
            },
            {
              key: "5",
              text: "Russian",
              value: "ru",
            },
          ]}
        />
      </Label>
      <div style={{ marginRight: "5px" }}></div>
      <Label>
        <Dropdown
          compact
          placeholder="USD"
          // selection
          onSearchChange={(e) => {}}
          options={[
            {
              key: "1",
              text: "WON",
              value: "won",
            },
            {
              key: "2",
              text: "USD",
              value: "usd",
            },
            {
              key: "3",
              text: "UAE",
              value: "uae",
            },
            {
              key: "4",
              text: "SOM",
              value: "som",
            },
            {
              key: "5",
              text: "RU",
              value: "ru",
            },
          ]}
        />
      </Label>
      {/* <VerticalLine class="vl" /> */}
      {/* <Link to="/login">
        <User size={28} color="black" />
        <Label style={{ cursor: "pointer" }}>{t("admin")}</Label>
      </Link> */}
    </Container>
  );
};

export default Topmenu;
