import React, { useContext } from "react";
import { Container, User, Buttons } from "../style/AccountsStyle";
import { Telegram } from "@styled-icons/remix-line/Telegram";
import { Instagram } from "@styled-icons/entypo-social/Instagram";
import { Whatsapp } from "@styled-icons/fa-brands/Whatsapp";
import { FacebookCircle } from "@styled-icons/boxicons-logos/FacebookCircle";
import { LangContext } from "../context/LangContext";
import { useTranslation } from "react-i18next";

const Accounts = () => {
  const data = useContext(LangContext);
  const { lang } = data;
  const { t, i18n } = useTranslation(lang);
  return (
    <Container>
      <User />
      <User.Title>{t("account.welcome")}</User.Title>
      <User.Description>{t("account.follow")}</User.Description>
      <Buttons>
        <a href="http://t.me/dxbbazaruzb">
          <Buttons.Telegram>
            <Telegram size={26} />
          </Buttons.Telegram>
        </a>
        <a href=" https://www.instagram.com/dxbbazaruzb/">
          <Buttons.Instagram>
            <Instagram size={20} />
          </Buttons.Instagram>
        </a>
        <a href=" https://www.instagram.com/dxbbazaruzb/">
          <Buttons.Whatsapp>
            <Whatsapp size={25} />
          </Buttons.Whatsapp>
        </a>

        <a href=" https://www.instagram.com/dxbbazaruzb/">
          <Buttons.FacebookCircle>
            <FacebookCircle size={25} />
          </Buttons.FacebookCircle>
        </a>
      </Buttons>
      {/* <Special>
        <Special.Title>Today's special offer</Special.Title>
        <Shoppiffy size={70} />
      </Special> */}
    </Container>
  );
};

export default Accounts;
