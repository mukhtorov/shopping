import React from "react";
import { Container, BodyContainer, BodyCarousel } from "../style/BodyStyle";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Imgs from "../moc/img/bg2.jpg";
import Sidebar from "./SideBar";
import MCotegory from "./MCotegory";
import Accounts from "./Accounts";
import { Items } from "../moc/Items";
const Body = () => {
  return (
    <Container>
      <Sidebar />
      <BodyContainer>
        <BodyCarousel>
          <Carousel infiniteLoop={true} autoPlay={true} transitionTime={1000}>
            {Items.map((item) => {
              return (
                <BodyCarousel.Content key={item.id}>
                  <img src={Imgs} alt="image" />
                </BodyCarousel.Content>
              );
            })}
          </Carousel>
        </BodyCarousel>
        <MCotegory />
      </BodyContainer>
      <Accounts />
    </Container>
  );
};

export default Body;
