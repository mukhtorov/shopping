import React, { Component } from "react";
import Products from "./Products";
import Body from "./Body";
import Process from "./Process";
import Footer from "./Footer";
import Navbar from "./NavBar";
import Topmenu from "./TopMenu";
const Home = () => {
  return (
    <>
      <Topmenu />
      <Navbar />
      <Body />
      <Products />
      <Process />
      <Footer />
    </>
  );
};

export default Home;
