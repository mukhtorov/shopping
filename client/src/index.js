import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import LangContect from "./context/LangContext";
import "./i18n";

ReactDOM.render(
  <div style={{ background: "white", height: "100%" }}>
    <React.StrictMode>
      <LangContect>
        <App />
      </LangContect>
    </React.StrictMode>
  </div>,
  document.getElementById("root")
);
