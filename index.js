const express = require("express");
const path = require("path");
const generatePassword = require("password-generator");
var cloudinary = require("cloudinary");
var cors = require("cors");

const app = express();
app.use(cors());
// Serve static files from the React app
app.use(express.static(path.join(__dirname, "client/build")));

cloudinary.config({
  cloud_name: "webbrain",
  api_key: "355669416718229",
  api_secret: "vtllKSih_ml2FBnjuhMby1QiNE4",
});

// home
// gettin all data
app.get("/api/test", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping",
      max_results: 100,
      context: true
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});


// gettin all data
app.get("/api", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping",
      max_results: 100,
      context: true

    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});


// men data
app.get("/api/men", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/men",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// women data
app.get("/api/women", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/women",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// watch data
app.get("/api/watch", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/watch",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// bag data
app.get("/api/bag", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/bag",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// cosmetics data
app.get("/api/cosmetic", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/beauty",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// food data
app.get("/api/food", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/food",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// electronics data
app.get("/api/electronic", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/electronic",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// sport data
app.get("/api/sport", (req, res) => {
  cloudinary.v2.api.resources(
    {
      type: "upload",
      prefix: "shopping/sport",
    },
    function (error, result) {
      console.log(error, result);
      res.send(result);
    }
  );
});

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/build/index.html"));
});

const port = process.env.PORT || 5005;
app.listen(port);

console.log(`Password generator listening on ${port}`);
